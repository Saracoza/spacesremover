#include "Helper.h"

#include <filesystem>

#include <Windows.h>
#include <ShlObj.h>

namespace fs = experimental::filesystem;

void utility::displayHelp()
{
	cout << "'Spaces Remover' options:" << endl;
	cout << "-> '?' OR 'help' OR '-help' -> Display help." << endl;
	cout << "-> '-e [e1] [e2] ...' -> Add extensions for processing. Default is 'txt'." << endl;
	cout << "-> '-d [d1] [d2] ...' -> Add directories for processing." << endl;
	cout << "-> '-r' -> Enable recursive search in directories." << endl;
	cout << "-> '-f [f1] [f2] ...' -> Add files for processing." << endl;
	cout << "-> '-o' -> Overwrite files. Default output folder is 'Documents\\Spaces Remover\\[filename].[extension]'." << endl;
}

void utility::Helper::addFilesFromDir(const wstring& dirPath, const bool& isRecursive, list<wstring>& files)
{
	for (const fs::directory_entry& dirEntry : fs::directory_iterator(dirPath)) {
		if (isRecursive && dirEntry.status().type() == fs::file_type::directory) {
			addFilesFromDir(dirEntry.path().wstring(), isRecursive, files);
		}
		else {
			files.push_back(dirEntry.path().wstring());
		}
	}
}

void utility::Helper::makeListOfFiles(const list<wstring>& dirs, const bool& isRecursive, list<wstring>& files)
{
	wstring_convert<codecvt_utf8<wchar_t>, wchar_t> converter;

	for (const wstring& dir : dirs) {
		if (fs::exists(fs::v1::path(dir))) {
			addFilesFromDir(dir, isRecursive, files);
		}
		else {
			string errorMessage;
			errorMessage
				.append("Can't open directory: ")
				.append(converter.to_bytes(dir))
				.append("!");
			cout << errorMessage << endl;
		}
	}
}

const wstring utility::Helper::extractExtension(const wstring& filename)
{
	return filename.substr(filename.find_last_of(L'.') + 1U);
}

const wstring utility::Helper::getOutputPath()
{
	HRESULT hresult;
	hresult = CoInitialize(NULL);
	if (hresult != S_OK) {
		throw exception("Can't get the path to Documents directory!");
	}
	PWSTR documentsPath;
	hresult = SHGetKnownFolderPath(FOLDERID_Documents, KF_FLAG_DEFAULT, NULL, &documentsPath);
	if (hresult != S_OK) {
		throw exception("Can't get the path to Documents directory!");
	}
	wstring outputPath(documentsPath);
	CoTaskMemFree(documentsPath);
	outputPath += L"\\Spaces Remover\\";

	if (fs::exists(fs::v1::path(outputPath)) == false) {
		fs::create_directory(outputPath);
	}

	return outputPath;
}

const wstring utility::Helper::getOutputName(const wstring& filename)
{
	wstring outputName = filename;

	size_t outputNameLength = outputName.length();
	for (string::size_type i = 0; i < outputNameLength; ++i) {
		if (outputName[i] == ':' || outputName[i] == '\\') {
			outputName[i] = '@';
		}
	}

	return outputName;
}