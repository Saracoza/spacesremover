#include "Helper.h"

#include <fstream>
#include <sstream>
#include <vector>
#include <thread>
#include <mutex>

mutex files_mtx;

void removeSpaces(wstring filename, const bool& isOverwrite, const wstring& outputPath)
{
	ifstream iFileStream(filename, ios::in);
	if (iFileStream.fail()) {
		string errorMessage;
		errorMessage
			.append("Can't open file: ")
			.append(wstring_convert<codecvt_utf8<wchar_t>, wchar_t>().to_bytes(filename))
			.append("!");
		throw exception(errorMessage.c_str());
	}


	string bufferLine;
	size_t bufferLineLength;
	size_t spacesCounter(0);
	size_t nlsCounter(0);
	string processedLine;
	stringstream tempBuffer;


	while (!iFileStream.eof()) {
		getline(iFileStream, bufferLine);
		bufferLineLength = bufferLine.length();

		for (string::size_type i = 0; i < bufferLineLength; ++i) {
			if (bufferLine[i] == ' ' || bufferLine[i] == '\t') {
				++spacesCounter;
			}
			else spacesCounter = 0;
		}

		processedLine = bufferLine.substr(0U, bufferLineLength - spacesCounter);
		spacesCounter = 0;

		if (processedLine.empty()) {
			++nlsCounter;
		}
		else {
			for (size_t i = 0; i < nlsCounter; ++i) {
				tempBuffer << endl;
			}
			nlsCounter = 0;

			tempBuffer << processedLine;
			tempBuffer << endl;
		}
	}


	ofstream oFileStream;
	if (isOverwrite) {
		oFileStream.open(filename, ios::out);
		oFileStream << tempBuffer.rdbuf();
	}
	else {
		oFileStream.open(outputPath + utility::Helper::getOutputName(filename), ios::out);
		oFileStream << tempBuffer.rdbuf();
	}


	iFileStream.close();
	oFileStream.close();
}

void doThreadWork(const list<wstring>* const exts, list<wstring>* files, const bool& isOverwrite, const wstring& outputPath)
{
	wstring filename;

	files_mtx.lock();
	while (!files->empty()) {
		filename = files->front();
		files->pop_front();
		files_mtx.unlock();

		for (const wstring& ext : (*exts)) {
			if (ext == utility::Helper::extractExtension(filename)) {
				try {
					removeSpaces(filename, isOverwrite, outputPath);
				}
				catch (exception e) {
					cout << e.what() << endl;
				}
				break;
			}
		}

		files_mtx.lock();
	}
	files_mtx.unlock();
}

int main(int argc, char* argv[])
{
	if (argc == 2 && (argv[1] == "?" || argv[1] == "help" || argv[1] == "-help")) {
		utility::displayHelp();
		return 0;
	}


	list<wstring> exts;
	list<wstring> dirs;
	bool isRecursive = false;
	list<wstring> files;
	bool isOverwrite = false;


	size_t argLen;
	size_t wcsLen;
	wchar_t** wcs_argv;
	wcs_argv = new wchar_t*[argc];
	for (int i = 0; i < argc; ++i) {
		argLen = strlen(argv[i]) + 1;
		wcs_argv[i] = new wchar_t[argLen];
		mbstowcs_s(
			&wcsLen,
			wcs_argv[i],
			argLen,
			argv[i],
			argLen - 1);
	}

	enum OptionType
	{
		extension,
		directory,
		file,
		none
	} optionType = none;

	for (int i = 1; i < argc; ++i) {
		if (!strcmp(argv[i], "-e")) {
			optionType = extension;
		}
		else if (!strcmp(argv[i], "-d")) {
			optionType = directory;
		}
		else if (!strcmp(argv[i], "-r")) {
			isRecursive = true;
		}
		else if (!strcmp(argv[i], "-f")) {
			optionType = file;
		}
		else if (!strcmp(argv[i], "-o")) {
			isOverwrite = true;
		}
		else {
			switch (optionType) {
			case extension: exts.push_back(wcs_argv[i]);	break;
			case directory: dirs.push_back(wcs_argv[i]);	break;
			case file:		files.push_back(wcs_argv[i]);	break;
			case none: {
				utility::displayHelp();
				return -1;
			} break;
			}
		}
	}

	if (exts.empty()) {
		exts.push_back(L"txt");
	}
	utility::Helper::makeListOfFiles(dirs, isRecursive, files);


	wstring outputPath;
	if (!isOverwrite) {
		try {
			outputPath = utility::Helper::getOutputPath();
		}
		catch (exception e) {
			cout << e.what() << endl;
			return -2;
		}
	}


	size_t threadsCount = files.size() < thread::hardware_concurrency() ? files.size() : thread::hardware_concurrency();
	vector<thread> threadPool;
	for (size_t i = 0; i < threadsCount; ++i) {
		threadPool.push_back(thread(doThreadWork, &exts, &files, isOverwrite, outputPath));
	}

	for (size_t i = 0; i < threadsCount; ++i) {
		threadPool[i].join();
	}


	for (int i = 0; i < argc; ++i) {
		delete[] wcs_argv[i];
	}
	delete[] wcs_argv;
	return 0;
}