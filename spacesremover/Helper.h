#pragma once

#include <list>
#include <string>
#include <codecvt>
#include <iostream>

using namespace std;

namespace utility
{
	void displayHelp();

	class Helper
	{
	private:
		static void addFilesFromDir(const wstring& dirPath, const bool& isRecursive, list<wstring>& files);
	public:
		static void makeListOfFiles(const list<wstring>& dirs, const bool& isRecursive, list<wstring>& files);
		static const wstring extractExtension(const wstring& filename);
		static const wstring getOutputPath();
		static const wstring getOutputName(const wstring& filename);
	};
}